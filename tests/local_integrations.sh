#!/bin/bash

myIP=`ifconfig en0 | grep 'inet '| awk '{ print $2 }'`

function onInterrupt {
  docker kill firefox_integrations
  docker rm firefox_integrations
}

trap onInterrupt EXIT

docker run -d -p 4501:5900 --name firefox_integrations --link physio_hub_1:hub selenium/node-firefox-debug
docker start firefox_integrations
sleep 10
case $1 in
  "--screenshot") NODE_ENV=$myIP SS=true ./node_modules/.bin/nightwatch -c conf/local.integrations.conf.js -e firefox ;;
  *)  NODE_ENV=$myIP ./node_modules/.bin/nightwatch -c conf/local.integrations.conf.js -e firefox
esac
docker kill firefox_integrations
docker rm firefox_integrations
