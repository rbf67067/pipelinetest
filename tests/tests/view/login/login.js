var makeScreenShot = require('../../makeScreenshot');

module.exports = function (browser) {
  console.log(process.env.NODE_ENV);
  browser
    .url('http://' + process.env.NODE_ENV + ':4000' + '/')
    .click('.rsq-app-button')
    .waitForElementVisible('body', 30000)
    .waitForElementVisible('.form-view-container', 30000)
    .clearValue('input[type=text]')
    .setValue('input[type=text]', 'registration@test.pl')
    makeScreenShot(browser, "Login_View", "view/Login")
    .click('.rsq-flat-button')
    .perform(function(){
      console.log('👍 login SUCCESS');
    });
}
