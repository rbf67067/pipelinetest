module.exports = function (browser, name, directory) {
  if(process.env.SS) {
    return browser
    .resizeWindow(1280, 800)
    .saveScreenshot('./screenshots/' + directory.toString() + '/' + Date.now().toString() + '_' + name + '_1280_800.png')
    .resizeWindow(1280, 1024)
    .saveScreenshot('./screenshots/' + directory.toString() + '/' + Date.now().toString() + '_' + name + '_1280_1024.png')
    .resizeWindow(1440, 900)
    .saveScreenshot('./screenshots/' + directory.toString() + '/' + Date.now().toString() + '_' + name + '_1440_900.png')
    .resizeWindow(1920, 1024)
    .saveScreenshot('./screenshots/' + directory.toString() + '/' + Date.now().toString() + '_' + name + '_1920_1024.png')
    .resizeWindow(2048, 1536)
    .saveScreenshot('./screenshots/' + directory.toString() + '/' + Date.now().toString() + '_' + name + '_2048_1536.png');
  }
  return browser
};
