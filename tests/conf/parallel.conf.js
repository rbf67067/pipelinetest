var config = require('../access_key');

nightwatch_config = {
  src_folders : [ "tests" ],

  selenium : {
    "start_process" : false,
    "host" : "hub-cloud.browserstack.com",
    "port" : 80
  },

  common_capabilities: {
    'build': 'nightwatch-browserstack',
    'browserstack.user': process.env.BROWSERSTACK_USERNAME || 'BROWSERSTACK_USERNAME',
    'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY || 'BROWSERSTACK_ACCESS_KEY',
    'browserstack.debug': true
  },

  test_settings: {
    default: {},
    chrome: {
      desiredCapabilities: {
        'javascriptEnabled': true,
        'build': 'nightwatch-browserstack',
        'browserstack.user': config.user,
        'browserstack.key': config.key,
        'browserstack.local' : true,
        'browserstack.localIdentifier' : 'Test123',
        'os': 'Windows',
        'os_version': '10',
        'browser': 'Chrome',
        'browser_version': '58.0',
        'resolution': '2048x1536'
      }
    },
    firefox: {
      desiredCapabilities: {
        'javascriptEnabled': true,
        'build': 'nightwatch-browserstack',
        'browserstack.user': config.user,
        'browserstack.key': config.key,
        'browserstack.local' : true,
        'browserstack.localIdentifier' : 'Test123',
        'os': 'Windows',
        'os_version': '10',
        'browser': 'Firefox',
        'browser_version': '54.0 beta',
        'resolution': '2048x1536'
      }
    },
    safari: {
      desiredCapabilities: {
        'javascriptEnabled': true,
        'build': 'nightwatch-browserstack',
        'browserstack.user': config.user,
        'browserstack.key': config.key,
        'browserstack.local' : true,
        'browserstack.localIdentifier' : 'Test123',
        'os': 'OS X',
        'os_version': 'Sierra',
        'browser': 'Safari',
        'browser_version': '10.0',
        'resolution': '2048x1536'
      }
    },
    edge: {
      desiredCapabilities: {
        'javascriptEnabled': true,
        'build': 'nightwatch-browserstack',
        'browserstack.user': config.user,
        'browserstack.key': config.key,
        'browserstack.local' : true,
        'browserstack.localIdentifier' : 'Test123',
        'os': 'Windows',
        'os_version': '10',
        'browser': 'Edge',
        'browser_version': '14.0',
        'resolution': '2048x1536'
      }
    }
  }
};

// Code to support common capabilites
for(var i in nightwatch_config.test_settings){
  var config = nightwatch_config.test_settings[i];
  config['selenium_host'] = nightwatch_config.selenium.host;
  config['selenium_port'] = nightwatch_config.selenium.port;
  config['desiredCapabilities'] = config['desiredCapabilities'] || {};
  for(var j in nightwatch_config.common_capabilities){
    config['desiredCapabilities'][j] = config['desiredCapabilities'][j] || nightwatch_config.common_capabilities[j];
  }
}

module.exports = nightwatch_config;
