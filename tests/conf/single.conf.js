var config = require('../access_key');

nightwatch_config = {
  src_folders : [ "tests" ],

  selenium : {
    "start_process" : false,
    "host" : "hub-cloud.browserstack.com",
    "port" : 80
  },

  test_settings: {
    default: {
      desiredCapabilities: {
        'javascriptEnabled': true,
        'build': 'nightwatch-browserstack',
        'browserstack.user': config.user,
        'browserstack.key': config.key,
        'browserstack.local' : true,
        'browserstack.localIdentifier' : 'Test123',
        'os': 'Windows',
        'os_version': '7',
        'browser': 'Chrome',
        'browser_version': '58.0',
        'resolution': '2048x1536'
      }
    }
  }
};

// Code to copy seleniumhost/port into test settings
for(var i in nightwatch_config.test_settings){
  var config = nightwatch_config.test_settings[i];
  config['selenium_host'] = nightwatch_config.selenium.host;
  config['selenium_port'] = nightwatch_config.selenium.port;
}

module.exports = nightwatch_config;
