const args = require('minimist')(process.argv);

let isProduction = args['grid'] === 'true';

// chrome setting
let FIREFOX_CONFIGURATION = {
	browserName: 'firefox',
	javascriptEnabled: true,
	acceptSslCerts: true
};

// default setting
let DEFAULT_CONFIGURATION = {
	launch_url: "http://localhost",
	selenium_port: 5000,
	selenium_host: "localhost",
	silent: true,
	screenshots: {
		enabled: true,
		path: "screenshots",
		on_failure: true,
		on_error: true
	},
	desiredCapabilities: FIREFOX_CONFIGURATION
};

let ENVIRONMENTS = {
	default: DEFAULT_CONFIGURATION,
	"firefox": {
		"desiredCapabilities": FIREFOX_CONFIGURATION
	}
};

let SELENIUM_CONFIGURATION = {
	"start_process": isProduction,
	"server_path": "node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-3.0.1.jar",
	"log_path": "logs",
	"host": "127.0.0.1",
	"port": 5000,
	"cli_args": {
		"trustAllSSLCertificates": true,
		"webdriver.chrome.driver": "node_modules/chromedriver/lib/chromedriver/chromedriver",
		"webdriver.gecko.driver": "node_modules/geckodriver/geckodriver"
	}
};

module.exports = {
	src_folders: ['tests/view'],
	selenium: SELENIUM_CONFIGURATION,
	test_settings: ENVIRONMENTS
};
