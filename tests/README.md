Przegladarki:
https://www.browserstack.com/automate/nightwatch#configure-capabilities

AccessKey:
https://www.browserstack.com/accounts/settings

nightwatch:
http://nightwatchjs.org/guide#parallel-running

* Jak zrobić testy na browserstacku
  * Robimy konto
  * Zmieniamy AccessKey w package.json
  * Zmieniamy AccessKey i usera access_key.js
  * (opcjonalnie npm install)
  * Odpalamy server sprawdzajacy w katalogu /tests "npm run remote-connect"
  * Odpalamy wszystkie testy npm run remote-all
  * (opcjonalnie odpalamy tylko jeden npm run remote-single)


* Komendy na testy zdalne:
  * npm run remote-connect (Połączenie TCP do server'a browserstacka)
  * npm run remote-single (Odpalenie testów tylko na chromie(os. WINDOWS))
  * npm run remote-all (Odpalenie testów tylko na chromie, firefoxie i innych (os. WINDOWS))


* Komendy na testy lokalne(na kontenerze):
  * npm run test (Uruchomienie wszystkich menu testów lokalnych)

* Jak zobaczyć live local testy
  * Pobieramy program do połączenia zdalnego z serverem linux (zajmuje on 6mb)
    https://www.realvnc.com/en/download/viewer/macos/
  * Po uruchomieniu w pasek z napisem "Enter a VNC Server address or search" wpisujemy "localhost:4501" lub "localhost:4502" (zalezy jakie testy robimy)
  * Wpisujemy hasło: "secret"
