PS3='Please enter your test choice: '
options=("Integrations tests" "Integrations tests + screenshots" "Views tests" "Views tests + screenshots" "Views&Integrations tests" "Views&Integrations tests + screenshots" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Integrations tests")
            chmod 777 local_integrations.sh && ./local_integrations.sh
            ;;
        "Integrations tests + screenshots")
            chmod 777 local_integrations.sh && ./local_integrations.sh --screenshot
            ;;
        "Views tests")
            chmod 777 local_views.sh && ./local_views.sh
            ;;
        "Views tests + screenshots")
            chmod 777 local_views.sh && ./local_views.sh --screenshot
            ;;
        "Views&Integrations tests")
            chmod 777 local_views.sh && chmod 777 local_integrations.sh && ./local_views.sh & ./local_integrations.sh
            ;;
        "Views&Integrations tests + screenshots")
            chmod 777 local_views.sh && chmod 777 local_integrations.sh && ./local_views.sh --screenshot & ./local_integrations.sh --screenshot
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done
