#!/bin/bash

myIP=`ifconfig en0 | grep 'inet '| awk '{ print $2 }'`

function onInterrupt {
  docker kill firefox_views
  docker rm firefox_views
}

trap onInterrupt EXIT

docker run -d -p 4502:5900 --name firefox_views --link physio_hub_1:hub selenium/node-firefox-debug:3.4
docker start firefox_views
sleep 10
case $1 in
  "--screenshot") NODE_ENV=$myIP SS=true ./node_modules/.bin/nightwatch -c conf/local.views.conf.js -e firefox ;;
  *)  NODE_ENV=$myIP ./node_modules/.bin/nightwatch -c conf/local.views.conf.js -e firefox
esac
docker kill firefox_views
docker rm firefox_views
