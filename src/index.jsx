import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './containers/App.jsx';
import store from './store/store';

import './styles/main.scss';

if (process.env.NODE_ENV !== 'production') {
  const { whyDidYouUpdate }: Object = require('why-did-you-update');
  let createClass: Function = React.createClass;
  Object.defineProperty(React, 'createClass', {
    set: nextCreateClass => {
      createClass = nextCreateClass;
    },
  });
  whyDidYouUpdate(React, { exclude: /(Route|Switch|Connect|Container)/ });
}

window.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('app'),
  );
});
