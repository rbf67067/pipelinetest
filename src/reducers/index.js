/* @flow */

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

const rootReducer: Object = combineReducers({
  routing: routerReducer,
});

export default rootReducer;
