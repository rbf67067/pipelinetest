/* @flow */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter, push } from 'react-router-redux';

const isDevelopment: boolean = process.env.NODE_ENV !== 'production';

import LoginContainer from './LoginContainer.jsx';

import { history } from '../store/store';

const App: App = () => {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/" component={LoginContainer} />
        <Route path="/login" component={LoginContainer} />
      </Switch>
    </ConnectedRouter>
  );
};

export default App;
