/* @flow */

import React, { PureComponent } from 'react';

import Login from 'Components/Login/Login.jsx';

type PropsType = {};

class LoginContainer extends PureComponent<PropsType> {
  render(): React$Element<typeof Login> {
    return (
      <Login />
    );
  }
}

export default LoginContainer;
