/* @flow */

import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const isProduction: boolean = process.env.NODE_ENV === 'production' ? true : false;

export const history: Object = createBrowserHistory();

const middlewareHistory: Function = routerMiddleware(history);

import rootReducer from '../reducers/index';

export const store: Object =
  createStore(rootReducer, isProduction ?
    applyMiddleware(thunk, middlewareHistory) :
    composeWithDevTools(applyMiddleware(thunk, middlewareHistory))
  );

export default store;
