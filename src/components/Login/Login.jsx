/* @flow */

import React, { PureComponent } from 'react';

type PropsType = {};

class Login extends PureComponent<PropsType> {
  render(): React$Element<'div'> {
    return (
      <div>Login Component</div>
    );
  }
}

export default Login;
