FROM nginx

COPY ./public/ /usr/share/nginx/html/

COPY ./public/default.conf /etc/nginx/conf.d/default.conf
