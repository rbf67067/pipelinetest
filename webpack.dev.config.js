var os = require('os');
var ifaces = os.networkInterfaces();
const path = require('path')
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');

const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT || 4000;

(function showIp() {
  Object.keys(ifaces).forEach(function (ifname) {
    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        return;
      }
      console.log('🛠 DEVELOPMENT \n');
      console.log('💻 http://localhost:' + PORT + ' \n ');
      console.log('📲 http://' + iface.address + ':' + PORT + ' \n ');
    });
  });
})();

const stats = {
  assets: true,
  children: false,
  chunks: false,
  hash: false,
  modules: false,
  publicPath: false,
  timings: true,
  version: false,
  warnings: true,
  colors: {
    green: '\u001b[32m',
  },
};


module.exports = function(env) {
  const plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new PreloadWebpackPlugin(),
    new ExtractTextPlugin({
      filename: 'style.css',
      allChunks: false,
    }),
  ];

  return {
    devtool: 'cheap-module-source-map',
    entry: {
      app: './src/index.jsx',
    },
    output: {
      path: path.join(__dirname, './public'),
      filename: '[name].bundle.js'
    },
    resolve: {
      alias: {
        Utils: path.resolve(__dirname, 'src/components/Utils/'),
        Components: path.resolve(__dirname, 'src/components/'),
        Modules: path.resolve(__dirname, 'src/modules/'),
        Containers: path.resolve(__dirname, 'src/containers/'),
        Actions: path.resolve(__dirname, 'src/actions/'),
        Languages: path.resolve(__dirname, 'src/languages/index.js'),
        Mocks: path.resolve(__dirname, 'mocks/'),
        Config: path.resolve(__dirname, 'src/config.js'),
        Svg: path.resolve(__dirname, 'src/components/Svg/'),
      },
    },
    module: {
      rules: [
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loader: 'file-loader?name=images/[name]-[hash].[ext]'
        },
        {
            test: /\.(woff|woff2|eot|ttf)$/i,
            loader: 'file-loader?name=fonts/[name]-[hash].[ext]'
        },
        {
          test: /\.s?css$/,
          exclude: /(node_modules)/,
          loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader?sourceMap',
        },
        {
          test: /\.jsx?$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: ["babel-preset-es2017", "babel-preset-flow"].map(require.resolve)
          }
        },
      ],
    },
    plugins: plugins,
    stats: stats,
    devServer: {
      contentBase: './public',
      publicPath: '/',
      historyApiFallback: true,
      port: PORT,
      host: HOST,
      hot: true,
      compress: false,
      stats: stats,
      disableHostCheck: true,
    },
  }
}
