const path = require('path')
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const UglifyEsPlugin = require('uglify-es-webpack-plugin');

const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT || 4000;

console.log("🚢 PRODUCTION ");

const stats = {
  assets: true,
  children: false,
  chunks: false,
  hash: false,
  modules: false,
  publicPath: false,
  timings: true,
  version: false,
  warnings: true,
  colors: {
    green: '\u001b[32m',
  },
};

module.exports = function(env) {
  const plugins = [
    new ExtractTextPlugin({
      filename: 'style.css',
      allChunks: false,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new UglifyEsPlugin({
      compress: {
        warnings: false,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        join_vars: true,
        if_return: true
      },
      output: {
        comments: false
      }
    }),
    new PreloadWebpackPlugin(),
  ];

  return {
    entry: {
      app: './src/index.jsx',
    },
    output: {
      path: path.join(__dirname, './public'),
      filename: '[name].bundle.js'
    },
    resolve: {
      alias: {
        Utils: path.resolve(__dirname, 'src/components/Utils/'),
        Components: path.resolve(__dirname, 'src/components/'),
        Modules: path.resolve(__dirname, 'src/modules/'),
        Containers: path.resolve(__dirname, 'src/containers/'),
        Actions: path.resolve(__dirname, 'src/actions/'),
        Languages: path.resolve(__dirname, 'src/languages/index.js'),
        Mocks: path.resolve(__dirname, 'mocks/'),
        Config: path.resolve(__dirname, 'src/config.js'),
        Svg: path.resolve(__dirname, 'src/components/Svg/'),
      },
    },
    module: {
      rules: [
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loader: 'file-loader?name=images/[name]-[hash].[ext]'
        },
        {
            test: /\.(woff|woff2|eot|ttf)$/i,
            loader: 'file-loader?name=fonts/[name]-[hash].[ext]'
        },
        {
          test: /\.s?css$/,
          exclude: /(node_modules)/,
          loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader?sourceMap',
        },
        {
          test: /\.jsx?$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: ["babel-preset-es2017", "babel-preset-flow"].map(require.resolve)
          }
        },
      ],
    },
    plugins: plugins,
    performance: {
      maxAssetSize: 300000,
      maxEntrypointSize: 300000,
      hints: 'warning',
    },
    stats: stats,
    devServer: {
      contentBase: './public',
      publicPath: '/',
      historyApiFallback: true,
      port: PORT,
      host: HOST,
      hot: false,
      compress: true,
      stats: stats,
      disableHostCheck: true,
    },
  }
}
